package com.softserve;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class View {
    static public void menu() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s;
        do {
            System.out.println("Select algorithm:");
            System.out.println("1 - Swap the first and last digits");
            System.out.println("2 - Add 1 to the beginning and to the end");
            System.out.println("3 - Lagrange theorem");
            System.out.println("Exit - Press 'E' ");
            s = reader.readLine();
            if (s.equals("1")) {
                Algorithm.swapDigits();
            } else if (s.equals("2")) {
                Algorithm.addDigits();
            } else if (s.equals("3")) {
                Algorithm.checkTheorem();
            }
        } while (!s.equalsIgnoreCase("e"));
    }
}
