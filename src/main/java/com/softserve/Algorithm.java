package com.softserve;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Algorithm {
    public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    static public void swapDigits() throws IOException {
        System.out.println("Please enter a value");
        StringBuilder builder = new StringBuilder();
        builder.append(reader.readLine());
        String start = builder.substring(0, 1);
        String end = builder.substring(builder.length() - 1);
        builder.deleteCharAt(0);
        builder.deleteCharAt(builder.length() - 1);
        builder.insert(0, end);
        builder.append(start);
        int a = Integer.parseInt(String.valueOf((builder)));
        System.out.println(a);
    }

    static public void addDigits() throws IOException {
        System.out.println("Please enter a value");
        StringBuilder builder = new StringBuilder();
        builder.append(reader.readLine());
        builder.insert(0, "1");
        builder.append("1");
        int a = Integer.parseInt(String.valueOf(builder));
        System.out.println(a);
    }

    static public void checkTheorem() throws IOException {
        System.out.println("Please enter a value");
        int n = Integer.parseInt(reader.readLine());
        int n2 = n;
        int a = (int) Math.sqrt(n2);
        n2 = n2 - a * a;
        int b = (int) Math.sqrt(n2);
        n2 = n2 - b * b;
        int c = (int) Math.sqrt(n2);
        n2 = n2 - c * c;
        int d = (int) Math.sqrt(n2);
        System.out.print(n+" = "+a+" "+b+" "+c+" "+d+" ");
        System.out.println();
    }
}
